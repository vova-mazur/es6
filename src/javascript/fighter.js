import ActiveFighterView from './views/activeFighterView';

const rand = (min, max) => {
	return Math.random() * (max - min) + min;
}

export default class Fighter extends ActiveFighterView {
	constructor(fighter, isRight) {
		super(fighter);

		const { name, health, attack, defense, source } = fighter;
		this.name = name;
		this.health = health;
		this.attack = attack;
		this.defense = defense;
		this.source = source;
		this.right = isRight;

		this.element.classList.add(this.right ? 'right' : 'left');
	}

	static minCriticalHitChance = 1;
	static maxCriticalHitChance = 2;

	getHitPower() {
		const criticalHitChance = rand(Fighter.minCriticalHitChance, Fighter.maxCriticalHitChance);
		const power = this.attack * criticalHitChance;

		return power;
	}

	getBlockPower() {
		const dodgeChance = rand(Fighter.minCriticalHitChance, Fighter.maxCriticalHitChance);
		const power = this.defense * dodgeChance;

		return power;
	}

	reloadHealthBar() {
		this.healthBar.value = this.health;
	}

	doDamage(f) {
		const hitPower = this.getHitPower();
		const blockPower = f.getBlockPower();
		const damage = hitPower - blockPower;

		if (damage > 0) {
			this._setTitle('Hit!');
			this.hit();
			f.block();
			const leftHealth = f.health - damage;
			if (leftHealth < 0) {
				alert(`${this.name} win`);
				window.location.reload()
			} else {
				f.health = leftHealth;
				f.reloadHealthBar();
			}
		} else {
			this._setTitle('Dodge!');
			this.hit();
			f.dodge();
		}
	}

	_setTitle(text) {
		const title = document.getElementById('title');
		title.innerHTML = text;
		setTimeout(() => title.innerHTML = '', 700);
	}
}
