import View from './view';
import FighterView from './fighterView';
import { fighterService } from './../services/fightersService';
import DetailsModal from './../modals/detailsModal';
import Fight from '../fight';

export default class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
    this.fightersDetailsMap = new Map();
    this.modal = new DetailsModal();
    this.fight = new Fight();

    this.createModalListener();
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  handleFighterClick(event, fighter) {
    const id = fighter._id;
    const info = this._getFighterDetails(id);
    if (event.target.checked) {
      info
        .then(fighter => this.fight.checkAndStart(fighter))
        .catch(err => console.error(err));
    } else {
      this.modal.show(info);
    }
  }

  async _getFighterDetails(id) {
    let fighterInfo;

    if (!this.fightersDetailsMap.has(id)) {
      try {
        fighterInfo = await fighterService.getFighterDetails(id);
        this.fightersDetailsMap.set(id, fighterInfo);
      } catch (error) {
        throw error;
      }
    } else fighterInfo = this.fightersDetailsMap.get(id);

    return fighterInfo;
  } 

  createModalListener() {
    this.modal.modal.addEventListener('close', event => {
      const newDetails = event.detail;
      const { _id } = newDetails;
      this.fightersDetailsMap.set(_id, newDetails);
    })
  }
}
