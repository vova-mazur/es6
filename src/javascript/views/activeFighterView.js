import FighterView from './fighterView';
import { setTimeout } from 'timers';

export default class ActiveFighterView extends FighterView {
	constructor(fighter, handleClick = null) {
		super(fighter, handleClick);

		this.element.removeChild(this.element.lastChild);
		this.healthBar = this.createHealthBar(fighter.health);
		this.element.append(this.healthBar);
		this.right = true;
	}

	createHealthBar(health) {
		const healthElement = this.createElement({ tagName: 'progress' });
		healthElement.value = health;
		healthElement.max = 100;

		return healthElement;
	}

	hit() {
		this._applyAnimation(`attack-${this.right ? 'left' : 'right'}`, 1000);
	}

	block() {
		this._applyAnimation('defence', 700)
	}

	dodge() {
		this._applyAnimation(`dodge-${this.right ? 'right' : 'left'}`, 1000);
	}

	_applyAnimation(name, mill) {
		this.element.classList.add(name, 'move');
		setTimeout(() => this.element.classList.remove(name, 'move'), mill);
	}
}
