import { callApi } from '../helpers/apiHelper';

const getInfoByEndpoint = async endpoint => {
  try {
    const apiResult = await callApi(endpoint, 'GET');
    return JSON.parse(atob(apiResult.content));
  } catch (error) {
    throw error;
  }
};

class FighterService {
  getFighters() {
    const endpoint = 'fighters.json';
    const info = getInfoByEndpoint(endpoint);
    return info;
  }

  getFighterDetails(_id) {
    const endpoint = `details/fighter/${_id}.json`;
    const info = getInfoByEndpoint(endpoint);
    return info;
  }
}

export const fighterService = new FighterService();
