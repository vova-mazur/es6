import View from './views/view';
import Fighter from './fighter';

export default class Fight extends View {
	constructor() {
		super();
	}

	firstFighter;
	secondFighter;

	countActiveFighters = 0;

	checkAndStart(fighter) {
		if (this.countActiveFighters == 0) {
			const f = new Fighter(fighter, false);
			this.firstFighter = f;
			this.countActiveFighters++;
		} else if (this.countActiveFighters == 1) {
			const f = new Fighter(fighter, true);
			this.secondFighter = f;
			this.start();
		}
	}

	fight(f1, f2) {
		let interval = setInterval(() => {
			f1.doDamage(f2);

			let f = f1;
			f1 = f2;
			f2 = f;
		}, 1300);
	}

	start(firstFighter = this.firstFighter, secondFighter = this.secondFighter) {
		const rootElement = document.getElementById('root');
		const fightersEl = document.getElementsByClassName('fighters')[0];
		fightersEl.style.display = 'none';
		const fightEl = this.createElement({ tagName: 'div', attributes: { id: 'fight' } });

		fightEl.style.display = 'flex';
		fightEl.append(firstFighter.element, secondFighter.element);
		const title = this.createElement({ tagName: 'div', attributes: { id: 'title' } });
		title.innerHTML = 'Lets Go!';
		rootElement.append(fightEl, title);

		this.fight(firstFighter, secondFighter);
	}
}
